#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 11:35:51 2017

"""

import numpy as np
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import StratifiedKFold
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from math import log
from sklearn.neighbors.kde import KernelDensity
from sklearn.metrics import accuracy_score

# Load data
data = np.loadtxt('TP1-data.csv',delimiter=',')

# Shuffle data
data = shuffle(data)

Ys = data[:,-1] # Labels
Xs = data[:,:-1] # Features

# Standardize features
means = np.mean(Xs,axis=0)
stdevs = np.std(Xs,axis=0)
Xs = (Xs-means)/stdevs

#Split data (stratified) for cross validation (X_r and Y_r) and testing (X_t and Y_t)
X_r,X_t,Y_r,Y_t = train_test_split(Xs, Ys, test_size=0.33,stratify = Ys)

#Stratified kfold to create multiple pairs of testing (tr_ix)
#and validation (va_ix) sets
folds = 5
kf = StratifiedKFold(Y_r, n_folds=folds)

#Draws graph 
def graph(x_axis_array,va_err_array,tr_err_array,xlabel,ylabel,graphName,fileName):
    va_plot, = plt.plot(x_axis_array, va_err_array,'r', label="Validation Error", marker='o')
    tr_plot, = plt.plot(x_axis_array, tr_err_array,'b', label="Training Error", marker='o')
    plt.legend(handles=[va_plot, tr_plot])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(graphName)
    plt.savefig(fileName,dpi=300)
    plt.show()
    


#Get best C for Logistic Regression classifier
def calc_fold_reg(X,Y, train_ix,valid_ix,C,feats=4):
    """return error for train and validation sets"""
    reg = LogisticRegression(C=C, tol=1e-10)
    reg.fit(X[train_ix],Y[train_ix])
    return 1 - reg.score(X[train_ix], Y[train_ix]),1 - reg.score(X[valid_ix], Y[valid_ix])

C=1
tr_err_reg_array =[]
va_err_reg_array =[]
x_axis_array = []
best_C = C
best_reg_error = 9001
for i in range(20):
    tr_err = va_err = 0
    #Cross-validation
    for tr_ix,va_ix in kf:
        r,v = calc_fold_reg(X_r,Y_r,tr_ix,va_ix,C)
        tr_err += r
        va_err += v
    #Get average errors
    avg_tr_err = tr_err/folds
    avg_va_err = va_err/folds
    #Get best C
    if avg_va_err < best_reg_error:
        best_reg_error = avg_va_err
        best_C = C
    tr_err_reg_array.append(avg_tr_err)
    va_err_reg_array.append(avg_va_err)
    x_axis_array.append(log(C))
    C*=2

#plots graph for Logistic Regression
graph(x_axis_array,va_err_reg_array,tr_err_reg_array,'log(C)','error','Logistic Regression','graph_reg.png')

#Get best k for K-Nearest Neighbours classifier
def calc_fold_knn(X,Y, train_ix,valid_ix,k,feats=4):
    """return error for train and validation sets"""
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X[train_ix],Y[train_ix])
    return 1 - knn.score(X[train_ix], Y[train_ix]),1 - knn.score(X[valid_ix], Y[valid_ix])

k=1
tr_err_knn_array =[]
va_err_knn_array =[]
x_axis_array = []
best_k = k
best_knn_error = 9001
while k <= 39:
    tr_err = va_err = 0
    #Cross-validation
    for tr_ix,va_ix in kf:
        r,v = calc_fold_knn(X_r,Y_r,tr_ix,va_ix,k)
        tr_err += r
        va_err += v
    #Get average errors
    avg_tr_err = tr_err/folds
    avg_va_err = va_err/folds
    #Get best k
    if avg_va_err < best_knn_error:
        best_knn_error = avg_va_err
        best_k = k
    tr_err_knn_array.append(avg_tr_err)
    va_err_knn_array.append(avg_va_err)
    x_axis_array.append(k)
    k += 2

#plots graph for Logistic Regression
graph(x_axis_array,va_err_knn_array,tr_err_knn_array,'k','error','k-Nearest Neighbour','graph_knn.png')

#Get best bandwidth paramater for KDEs used in Naive Bayes classifier
def get_c_probs(Y):
    """Calculate prior probability of belonging to class"""    
    c_probs = [0,0]
    for i in range(len(Y)):
        c_probs[int(Y[i])] += 1
    c_probs[0] /= len(Y)
    c_probs[1] /= len(Y)
    return c_probs


def get_kdes(X, Y, bw):
    """Get KDEs for given data"""    
    #Split classes
    X1 = X[:,[0]]
    X2 = X[:,[1]]
    X3 = X[:,[2]]
    X4 = X[:,[3]]
    
    #Split features and create KDEs
    kdes = []
    kde0 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde0.fit(X1[Y[:]<1]))
    kde1 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde1.fit(X1[Y[:]>0]))
    kde2 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde2.fit(X2[Y[:]<1]))
    kde3 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde3.fit(X2[Y[:]>0]))
    kde4 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde4.fit(X3[Y[:]<1]))
    kde5 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde5.fit(X3[Y[:]>0]))
    kde6 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde6.fit(X4[Y[:]<1]))
    kde7 = KernelDensity(kernel='gaussian', bandwidth=bw)
    kdes.append(kde7.fit(X4[Y[:]>0]))
        
    return kdes


def train_nb(X, Y, bandwidth):
    """Train Naive Bayes Classifier with given bandwidth for KDEs"""
    return get_c_probs(Y),get_kdes(X, Y, bandwidth)


def predict_nb(c_probs, kdes, X):
    """Predict using Naive Bayes Classifier"""
    c0x1 = kdes[0].score_samples(X[:,[0]])
    c0x2 = kdes[2].score_samples(X[:,[1]])
    c0x3 = kdes[4].score_samples(X[:,[2]])
    c0x4 = kdes[6].score_samples(X[:,[3]])    
    c1x1 = kdes[1].score_samples(X[:,[0]])
    c1x2 = kdes[3].score_samples(X[:,[1]])
    c1x3 = kdes[5].score_samples(X[:,[2]])
    c1x4 = kdes[7].score_samples(X[:,[3]])
    
    pred_y = []
    for i in range(len(X)):
        #Predict class to which the point belongs
        C0 = log(c_probs[0]) + c0x1[i] + c0x2[i] + c0x3[i] + c0x4[i]
        C1 = log(c_probs[1]) + c1x1[i] + c1x2[i] + c1x3[i] + c1x4[i]
        if C0 > C1:
            pred_y.append(0)
        else:
            pred_y.append(1)
    
    return pred_y


def calc_fold_nb(X,Y, train_ix,valid_ix,bandwidth,feats=4):
    """return error for train and validation sets"""
    c_probs, kdes = train_nb(X[train_ix], Y[train_ix], bandwidth)
    train_pred_Y = predict_nb(c_probs, kdes, X[train_ix])
    valid_pred_Y = predict_nb(c_probs, kdes, X[valid_ix])
    return 1 - accuracy_score(Y[train_ix], train_pred_Y),1 - accuracy_score(Y[valid_ix], valid_pred_Y)


bandwidth=0.01
tr_err_nb_array =[]
va_err_nb_array =[]
x_axis_array = []
best_bandwidth = bandwidth
best_nb_error = 9001
while bandwidth <= 1.0:
    tr_err = va_err = 0
    #Cross-validation
    for tr_ix,va_ix in kf:
        r,v = calc_fold_nb(X_r,Y_r,tr_ix,va_ix,bandwidth)
        tr_err += r
        va_err += v
    #Get average errors
    avg_tr_err = tr_err/folds
    avg_va_err = va_err/folds    
    #Get best bandwidth
    if avg_va_err < best_nb_error:
        best_nb_error = avg_va_err
        best_bandwidth = bandwidth
    tr_err_nb_array.append(avg_tr_err)
    va_err_nb_array.append(avg_va_err)
    x_axis_array.append(bandwidth)
    bandwidth += 0.02


#plots graph for Logistic Regression
graph(x_axis_array,va_err_nb_array,tr_err_nb_array,'bandwidth','error','Naive Bayes','graph_nb.png')


#Print best parameters
print("Best parameters > Logistic Regression, C =",best_C,"; k-Nearest Neighbours, k =",best_k,"; Naive Bayes, bandwidth =",best_bandwidth)

#Final tests
def test_final_reg(train_X,train_Y,test_X,test_Y, C):
    """Perform final test with Logistic Regression classifier
    Returns the test error and predicted labels
    """
    reg = LogisticRegression(C=C, tol=1e-10)
    reg.fit(train_X,train_Y)
    return 1 - reg.score(test_X, test_Y),reg.predict(test_X)

def test_final_knn(train_X,train_Y,test_X,test_Y, k):
    """Perform final test with k-Nearest Neighbours classifier
    Returns the test error and predicted labels
    """
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(train_X,train_Y)
    return 1 - knn.score(test_X, test_Y),knn.predict(test_X)

def test_final_nb(train_X,train_Y,test_X,test_Y, bandwidth):
    """Perform final test with Naive Bayes classifier
    Returns the test error and predicted labels
    """
    c_probs, kdes = train_nb(train_X, train_Y, bandwidth)
    test_pred_Y = predict_nb(c_probs, kdes, test_X)
    return 1 - accuracy_score(test_Y, test_pred_Y),test_pred_Y

reg_test_error,reg_pred_Y = test_final_reg(X_r, Y_r, X_t, Y_t, best_C)
knn_test_error,knn_pred_Y = test_final_knn(X_r, Y_r, X_t, Y_t, best_k)
nb_test_error,nb_pred_Y = test_final_nb(X_r, Y_r, X_t, Y_t, best_bandwidth)

print("Final test errors > Logistic Regression:",reg_test_error,"; k-Nearest Neighbours:",knn_test_error,"; Naive Bayes:",nb_test_error)

#McNemar test
def mcnemar_test(Y,Y1,Y2):
    e01=0
    e10=0
    for i in range(len(Y)):
        if Y1[i]!=Y2[i]:
            if Y1[i]!=Y[i]:
                e01+=1
            else:
                e10+=1
                
    print('E01:',e01)
    print('E10:',e10)
    result = ((np.absolute(e01-e10)-1)**2)/(e01+e10)
    
    return result

#Logistic vs KNN result
print('LR vs KNN:',mcnemar_test(Y_t,reg_pred_Y,knn_pred_Y))
#KNN vs Naive Bayes result
print('KNN vs NB:',mcnemar_test(Y_t,knn_pred_Y,nb_pred_Y))
#Nayve Bays vs Logistic result
print('NB vs LR:',mcnemar_test(Y_t,nb_pred_Y,reg_pred_Y))
